<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test','UsersController@test');
Route::post('/fblogin','UsersController@fbLogin')->middleware('requestHandler:FacebookLoginRequest');
Route::post('/add/bet','UsersBetsController@addBet')->middleware('requestHandler:AddUserBetRequest');

Route::get('/get/pending_bets','UsersBetsController@pendingBets')->middleware('requestHandler:PendingBetsRequest');
Route::get('/get/completed_bets','UsersBetsController@completedBets');

Route::post('/bet/request','UsersBetsController@acceptChallenge')->middleware('requestHandler:AcceptChallengeRequest');

Route::get('/search/user','UsersController@searchUser')->middleware('requestHandler:SearchUserRequest');


Route::get('/get/notifications','UsersBetsController@getNotifications')->middleware('requestHandler:NotificationsRequest');


Route::get('/get/friends_bets','UsersBetsController@friendsBets')->middleware('requestHandler:FriendsBetsRequest');
Route::get('/get/my_bets','UsersBetsController@myBets')->middleware('requestHandler:MyBetsBetsRequest');
Route::get('/get/public_bets','UsersBetsController@publicBets')->middleware('requestHandler:PublicBetsRequest');


Route::get('/cron','UsersController@getUsers');

Route::post('/logout','UsersController@userLogout')->middleware('requestHandler:LogOutRequest');

