<?php

namespace App\Http\Controllers;


use App\Http\LmResponse;
use App\Http\Response;
use App\Libs\Auth\Auth;
use App\Libs\Auth\LmAuth;
use App\Libs\Helpers\Helper;
use App\Repositories\FacebookFriendsRepository;
use App\Repositories\InviteUsersRepository;
use App\Repositories\MedicalDocumentsRepository;
use App\Repositories\NotificationsRepository;
use App\Repositories\UsersBetsRepository;
use App\Repositories\UsersRepository;
use App\User;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Facebook\Facebook;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Requests\AcceptChallengeRequest;
use Requests\AddMedicalDocumentRequest;
use Requests\AddUserBetRequest;
use Requests\BalanceHistoryRequest;
use Requests\ChangePasswordRequest;
use Requests\EditProfileRequest;
use Requests\FacebookLoginRequest;
use Requests\FilterUsersRequest;
use Requests\ForgotPasswordRequest;
use Requests\FriendsBetsRequest;
use Requests\GetTracksRequest;
use Requests\GetUsersRequest;
use Requests\InviteAcceptRequest;
use Requests\LegalDetailsRequest;
use Requests\LogOutRequest;
use Requests\MyBetsBetsRequest;
use Requests\NotificationsCountRequest;
use Requests\NotificationsRequest;
use Requests\PageRatingRequest;
use Requests\PriceFilterRequest;
use Requests\PublicBetsRequest;
use Requests\ReceivedInvitesRequest;
use Requests\Request;
use Requests\SentInvitesRequest;
use Requests\SoundCloudLinkRequest;
use Requests\StripeRequest;
use Requests\SubscriptionRequest;
use Requests\SwitchProfileRequest;
use Requests\TestRequest;
use Requests\UpdateLocationRequest;
use Requests\UpdateProfileRequest;
use Requests\UserInviteRequest;




class UsersBetsController
{

    public $fb = null;
    public $response = null;
    public $lm_response = null;
    private $usersRepo = null;
    private $userBetRepo = null;
    private $fbFriendsRepo = null;

    public function __construct(UsersRepository $usersRepo,UsersBetsRepository $userBetRepo,FacebookFriendsRepository $fbFriendsRepo)
    {
        $this->response = new Response();
        $this->usersRepo = $usersRepo;
        $this->userBetRepo = $userBetRepo;
        $this->fbFriendsRepo = $fbFriendsRepo;
    }
    function test()
    {
        return $this->response->respond();
    }
    public function addBet(AddUserBetRequest $request)
    {
        try {
            $bet = $this->userBetRepo->insert($request->newBet());
            $receiver = $this->usersRepo->findById($request->input('receiver_id'));
            PushNotification::app($receiver->device_type)
                ->to($receiver->device_token)
                ->send(Auth::user()->first_name.' '.Auth::user()->last_name.' '.'invited you a new bet ',array(
//                    'sender_name'=>Auth::user()->first_name.' '.Auth::user()->last_name,
                    'data' => array(
                        'sender_id' => Auth::user()->id,
                        'sender_first_name'=>Auth::user()->first_name,
                        'sender_last_name'=>Auth::user()->last_name,
                        'sender_bet' => $bet->sender_bet,
                        'receiver_bet' => $bet->receiver_bet,
                        'sender_picture' => Auth::user()->picture,
                        'end_date' => $bet->end_date,
                        'privacy_type' => $bet->privacy_type
                    )
                ));
            return $this->response->respond(['data' => $bet]);

        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function pendingBets()
    {
        try {
           return $this->response->respond(['data' => $this->transformDate($this->usersRepo->getPendingBets())]);
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function completedBets()
    {
        try {
            return $this->response->respond(['data' => $this->usersRepo->getCompletedBets()]);
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function getNotifications(NotificationsRequest $request)
    {
        try {
            $number = $request->get('number');
            if(!isset($number) || $number <= 0) {$number = 1;}
            $notifications = $this->usersRepo->getAllNotifications($number);
            return $this->response->respond(['data' => [
                'notifications' => $notifications,
                'page' => (count($notifications) >= 10)?$number+1:null
            ]]);
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function acceptChallenge(AcceptChallengeRequest $request)
    {
        try {
            $this->userBetRepo->acceptChallenge($request->input('bet_id'),$request->input('status'));
            return $this->response->respond();
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function friendsBets(FriendsBetsRequest $request)
    {
        try {
            $number = $request->get('number');
            if(!isset($number) || $number <= 0) {$number = 1;}
            $bets = $this->fbFriendsRepo->getFriendsBets($request->get('filter_param'),$number);
           return $this->response->respond(['data' => [
               'bets' => $this->transformDate($bets),
               'page' => (count($bets) >= 25)?$number+1:null
           ]]);
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function myBets(MyBetsBetsRequest $request)
    {
        try {
            $number = $request->get('number');
            if(!isset($number) || $number <= 0) {$number = 1;}
            $bets = $this->userBetRepo->getMyBets($request->get('filter_param'),$number);
            return $this->response->respond(['data' => [
                'bets' => $this->transformDate($bets),
                'page' => (count($bets) >= 25)?$number+1:null
            ]]);
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function publicBets(PublicBetsRequest $request)
    {
        try {
            $number = $request->get('number');
            if(!isset($number) || $number <= 0) {$number = 1;}
            $bets = $this->usersRepo->getPublicBets($request->get('filter_param'),$number);
            return $this->response->respond(['data' => [
                'bets' => $this->transformDate($bets),
                'page' => (count($bets) >= 25)?$number+1:null
            ]]);
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }

    public function transformDate($users)
    {
        $updated_user = [];
        foreach ($users as $user)
        {
            $cur_date = date('Y-m-d');
            $datetime1 = date_create($cur_date);
            $datetime2 = date_create($user->end_date);
            $var = date_diff($datetime1,$datetime2);
            $user->end_date = $var->days;
            array_push($updated_user,$user);
        }
        return $updated_user;
    }
}