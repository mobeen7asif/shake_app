<?php

namespace App\Http\Controllers;


use App\FbFriend;
use App\Http\LmResponse;
use App\Http\Response;
use App\Libs\Auth\Auth;
use App\Libs\Auth\LmAuth;
use App\Libs\Helpers\Helper;
use App\Repositories\FacebookFriendsRepository;
use App\Repositories\InviteUsersRepository;
use App\Repositories\MedicalDocumentsRepository;
use App\Repositories\NotificationsRepository;
use App\Repositories\UsersRepository;
use App\User;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Facebook\Facebook;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Requests\AddMedicalDocumentRequest;
use Requests\BalanceHistoryRequest;
use Requests\ChangePasswordRequest;
use Requests\EditProfileRequest;
use Requests\FacebookLoginRequest;
use Requests\FilterUsersRequest;
use Requests\ForgotPasswordRequest;
use Requests\GetTracksRequest;
use Requests\GetUsersRequest;
use Requests\InviteAcceptRequest;
use Requests\LegalDetailsRequest;
use Requests\LogOutRequest;
use Requests\NotificationsCountRequest;
use Requests\NotificationsRequest;
use Requests\PageRatingRequest;
use Requests\PriceFilterRequest;
use Requests\ReceivedInvitesRequest;
use Requests\Request;
use Requests\SearchUserRequest;
use Requests\SentInvitesRequest;
use Requests\SoundCloudLinkRequest;
use Requests\StripeRequest;
use Requests\SubscriptionRequest;
use Requests\SwitchProfileRequest;
use Requests\TestRequest;
use Requests\UpdateLocationRequest;
use Requests\UpdateProfileRequest;
use Requests\UserInviteRequest;




class UsersController
{

    public $fb = null;
    public $response = null;
    public $lm_response = null;
    private $usersRepo = null;
    public $medicalDocRepo = null;
    private $fbFriendsRepo = null;

    public function __construct(UsersRepository $usersRepo,FacebookFriendsRepository $fbFriendsRepo)
    {
        $this->response = new Response();
        $this->usersRepo = $usersRepo;
        $this->fbFriendsRepo = $fbFriendsRepo;

    }
    function test()
    {
        return $this->response->respond();
    }
    public function fbLogin(FacebookLoginRequest $request,Facebook $fb)
    {
        try {

            $user = $this->usersRepo->checkExistingUser($request->input('access_token'),$fb);
            if($user == null)
            {
                $user = $this->usersRepo->insert($request->newUser($fb));
                $this->getFacebookFriends($user);
                return $this->response->respond(['data' => $user]);
            }
            else{
                $this->usersRepo->updateWhere(['fb_id' => $user->fb_id],['access_token' => $request->input('access_token'),'device_token' => $request->input('device_token'),'device_type' => $request->input('device_type')]);
                $this->getFacebookFriends($this->usersRepo->findByFbId($user->fb_id));
                return $this->response->respond(['data' => $this->usersRepo->findByFbId($user->fb_id)]);
            }
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function searchUser(SearchUserRequest $request)
    {
        try {
           return $this->response->respond(['data' => $this->usersRepo->getUsersBySearch($request->get('key'))]);
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
    public function getUsers()
    {
        $users = $this->usersRepo->all();
        foreach ($users as $user){
            $this->getFacebookFriends($user);
        }
    }
    public function getFacebookFriends($user)
    {
        $fb = new Facebook();
        $response = $fb->get('/me/friends?fields=id,name',$user->access_token);
        $friends = $response->getDecodedBody();
        $this->fbFriendsRepo->deleteFbFriends($user->id);
        if(isset($friends))
        {
            foreach ($friends['data'] as $friend)
            {
                $attributes = array('name' => $friend['name'],'fb_id' => $friend['id'],'user_id' => $user->id);
                $this->fbFriendsRepo->insert($attributes);
            }
        }
    }
    public function userLogout()
    {
        try {
           $this->usersRepo->updateWhere(['id' => Auth::user()->id],['access_token' => '']);
            return $this->response->respond();
        }
        catch(\Exception $e){
            return $this->response->respondInternalServerError($e->getMessage());
        }
    }
}