<?php

namespace Requests;

use App\Libs\Auth\Auth;
use App\User;
use Facebook\Facebook;
use Requests\Request;

class AddUserBetRequest extends Request
{
    public function __construct(){
        parent::__construct();

        $this->authenticatable = true;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [

        ];
    }

    public function transform()
    {
        return [

        ];
    }

    public function newBet()
    {
        $attributes = array('sender_id' => Auth::user()->id,'receiver_id' => $this->input('receiver_id'),
            'sender_bet' => $this->input('sender_bet'),
            'receiver_bet' => $this->input('receiver_bet'),
            'end_date' => $this->input('end_date'),
            'complete_status' => 0,
            'privacy_type' => $this->input('privacy_type'),
        );
        return $attributes;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'receiver_id' => 'required|exists:users,id',
            'end_date' => 'required|date_format:Y-m-d',
        ];
    }



}
