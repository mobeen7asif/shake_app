<?php

namespace Requests;

use Requests\Request;

class AcceptChallengeRequest extends Request
{
    public function __construct(){
        parent::__construct();

        $this->authenticatable = true;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bet_id' => 'required|exists:user_bets,id',
            'status' => 'required'
        ];
    }
}
