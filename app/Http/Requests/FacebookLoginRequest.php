<?php

namespace Requests;

use App\User;
use Facebook\Facebook;
use Requests\Request;

class FacebookLoginRequest extends Request
{
    public function __construct(){
        parent::__construct();

        $this->authenticatable = false;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'email.unique'=>"This email already exists",
            'device_type' => 'required',
            'device_token' => 'required'
        ];
    }

    public function transform()
    {
        return [

        ];
    }

    public function newUser(Facebook $fb)
    {
        $access_token = $this->input('access_token');
        $response = $fb->get('/me?fields=id,first_name,last_name,email,picture.width(200).height(200)',$access_token);
        $userNode = $response->getGraphUser();
        $picture_obj = $userNode->getField('picture');
        $picture = $picture_obj['url'];
        $attributes = array('access_token' => $access_token,'first_name' => $userNode->getField('first_name'),
            'last_name' => $userNode->getField('last_name'),
            'picture' => $picture,
            'device_type' => $this->input('device_type'),
            'device_token' => $this->input('device_token'),
            'fb_id' => $userNode->getField('id'),
            'email' => $userNode->getField('email'),
        );
        return $attributes;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'access_token' => 'required',
            'device_token' => 'required',
            'device_type' => 'required',

        ];
    }



}
