<?php
/**
 * Created by PhpStorm.
 * user: nomantufail
 * Date: 10/10/2016
 * Time: 10:13 AM
 */

namespace App\Repositories;

use App\Events\UserRegistered;
use App\FbFriend;
use App\Libs\Auth\Auth;
use App\Models\BlockedUser;
use App\Models\InviteUser;
use App\Models\LikedUser;
use App\Models\Notification;
use App\Models\UserInterests;
use App\User;
use App\UserBET;
use Carbon\Carbon;
use Facebook\Facebook;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class UsersRepository extends Repository
{
    public function __construct()
    {
        $this->setModel(new User());
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function store($user)
    {
        return $this->getModel()->create($user);
    }




//    public function updateWhere($where, $data)
//    {
//        return $this->getModel()->where($where)->update($data);
//    }
    public function updateWhere($where, $attrs)
    {
        $query = $this->getModel();
        foreach ($where as $key => $value) {
            $query = $query->where($key, '=', $value);
        }
        return $query->update($attrs);
    }


    public function getByIds($ids = [])
    {
        $lat1 = Auth::user()->latitude;
        $lon1 = Auth::user()->longitude;
        $usersTable = $this->getModel()->getTable();
        return $this->getModel()->select("*", DB::raw("(((acos(sin((" . $lat1 . "*pi()/180)) *
            sin(($usersTable.latitude*pi()/180))+cos((" . $lat1 . "*pi()/180)) *
            cos(($usersTable.latitude*pi()/180)) * cos(((" . $lon1 . "- $usersTable.longitude)*
            pi()/180))))*180/pi())*60*1.1515) as distance"))
            ->whereIn('id', $ids)
            ->get();
    }

    public function findByEmail($email)
    {
        $usersTable = $this->getModel()->getTable();
        return $this->getModel()->where('email', $email)
            ->first();
    }

    public function findByUserName($user_name)
    {
        return $this->getModel()->where('user_name', $user_name)
            ->first();
    }


    public function findByFbId($fbid)
    {
        return $this->getModel()->where('fb_id', $fbid)->first();
    }

    public function findByToken($token)
    {
        $usersTable = $this->getModel()->getTable();
        return $this->getModel()->where('access_token', $token)
            ->first();
    }

    private function getUserTableFields()
    {
        $userFields = [];
        collect($this->getModel()->fields())->each(function ($value, $key) use (&$userFields) {
            $userFields[] = "users." . $value;
        });
        return $userFields;
    }
    public function checkOldPassword($old_password)
    {
        return $this->getModel()->select('password')->where('password',bcrypt($old_password))->first();
    }

    public function getPendingBets()
    {
        $usersTable = $this->getModel()->getTable();
        $user_bets_Table = (new UsersBetsRepository(new UserBET()))->getModel()->getTable();

        return $this->getModel()->select($user_bets_Table.'.sender_bet',$user_bets_Table.'.receiver_bet',$user_bets_Table.'.privacy_type',$user_bets_Table.'.end_date',$usersTable.'.first_name',$usersTable.'.last_name')->
            where($user_bets_Table.'.sender_id', Auth::user()->id)
            ->where($user_bets_Table.'.complete_status' ,0)
            ->orWhere($user_bets_Table.'.complete_status' ,1)
            ->leftJoin($user_bets_Table,$usersTable.'.id','=',$user_bets_Table.'.receiver_id')
            ->get();
    }

    public function getCompletedBets()
    {
        $usersTable = $this->getModel()->getTable();
        $user_bets_Table = (new UsersBetsRepository(new UserBET()))->getModel()->getTable();

        return $this->getModel()->select($user_bets_Table.'.sender_bet',$user_bets_Table.'.receiver_bet',$user_bets_Table.'.privacy_type',$user_bets_Table.'.end_date',$usersTable.'.first_name',$usersTable.'.last_name')->
        where($user_bets_Table.'.sender_id', Auth::user()->id)
            ->where($user_bets_Table.'.complete_status' ,1)
            ->where($user_bets_Table.'.end_date','<=', date('Y-m-d'))
            ->leftJoin($user_bets_Table,$usersTable.'.id','=',$user_bets_Table.'.receiver_id')
            ->get();
    }
    public function getAllNotifications($number)
    {
        $usersTable = $this->getModel()->getTable();
        $user_bets_Table = (new UsersBetsRepository(new UserBET()))->getModel()->getTable();

        return $this->getModel()->select($user_bets_Table.'.id as bet_id',$user_bets_Table.'.sender_bet',$user_bets_Table.'.receiver_bet',$user_bets_Table.'.privacy_type',$user_bets_Table.'.end_date',$usersTable.'.id as sender_id',$usersTable.'.first_name as sender_first_name',$usersTable.'.last_name as sender_last_name',$usersTable.'.picture as sender_picture')->
        where($user_bets_Table.'.receiver_id', Auth::user()->id)
            ->where($user_bets_Table.'.complete_status' ,0)
            ->leftJoin($user_bets_Table,$usersTable.'.id','=',$user_bets_Table.'.sender_id')
            ->offset($number*10-10)
            ->limit(10)
            ->orderBy($user_bets_Table.'.created_at','desc')
            ->get();
    }

    public function checkExistingUser($access_token , Facebook $fb)
    {
        $response = $fb->get('/me?fields=id',$access_token);
        $userNode = $response->getGraphUser();
        $fb_id = $userNode->getField('id');
        $user = $this->findByFbId($fb_id);
        return $user;
    }

    public function getUsersBySearch($key)
    {
        if(isset($key)){
            return $this->getModel()
                ->where('first_name', 'like', '%'.$key.'%')
                ->orWhere('last_name', 'like', '%'.$key.'%')
                ->where('id', '<>' , Auth::user()->id)
                ->get();
        }else{
            $users_table = $this->getModel()->getTable();
            $fb_friends_table = (new FacebookFriendsRepository(new FbFriend()))->getModel()->getTable();
            $query = (new FbFriend())->select('users.*')
                ->where($users_table.'.id' , '<>' , null)
                ->where($fb_friends_table.'.user_id' , '=' , Auth::user()->id)
                ->leftJoin($users_table, $users_table.'.fb_id' , '=' , $fb_friends_table.'.fb_id');
            return $query->get();
        }

    }

    public function getPublicBets($filter_param,$number)
    {
        $users_table = $this->getModel()->getTable();
        $user_bets_Table = (new UsersBetsRepository(new UserBET()))->getModel()->getTable();
        $query = (new UserBET())->select(
            $user_bets_Table.'.sender_bet',$user_bets_Table.'.receiver_bet',$user_bets_Table.'.privacy_type',$user_bets_Table.'.end_date',
            'senders.first_name as sender_first_name','senders.last_name as sender_last_name','senders.picture as sender_picture',
            'receivers.first_name as receiver_first_name','receivers.last_name as receiver_last_name','receivers.picture as receiver_picture')
            ->where($user_bets_Table.'.id', '<>' ,null)
            ->where($user_bets_Table.'.sender_id' , '<>' , Auth::user()->id)
            ->where($user_bets_Table.'.privacy_type', '=' ,'Public')
            ->where($user_bets_Table.'.privacy_type' , '<>' , 'Invite only')
            ->leftJoin($users_table.' as senders','senders.id','=',$user_bets_Table.'.sender_id')
            ->leftJoin($users_table.' as receivers','receivers.id','=',$user_bets_Table.'.receiver_id')
            ->offset($number*25-25)
            ->limit(25);
        if($filter_param == 'all')
        {
            $query = $query->orderBy($user_bets_Table.'.created_at','desc');
            return $query->get();
        }
        if($filter_param == 'pending')
        {
            $query = $query->where($user_bets_Table.'.complete_status' ,0)
                ->orWhere($user_bets_Table.'.complete_status' ,1);
            return $query->get();
        }
        if($filter_param == 'completed')
        {
            $query = $query->where($user_bets_Table.'.complete_status' ,1)
                ->where($user_bets_Table.'.end_date','<=', date('Y-m-d'));
            return $query->get();
        }
        return  $query->orderBy($user_bets_Table.'.created_at','desc')->get();


    }




}





