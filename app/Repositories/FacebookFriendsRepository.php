<?php
/**
 * Created by PhpStorm.
 * user: nomantufail
 * Date: 10/10/2016
 * Time: 10:13 AM
 */

namespace App\Repositories;

use App\Events\UserRegistered;
use App\FbFriend;
use App\Libs\Auth\Auth;
use App\Models\BlockedUser;
use App\Models\InviteUser;
use App\Models\LikedUser;
use App\Models\Notification;
use App\Models\UserInterests;
use App\User;
use App\UserBET;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class FacebookFriendsRepository extends Repository
{
    public function __construct()
    {
        $this->setModel(new FbFriend());
    }

    /**
     * @param User $user
     * @return mixed
     */

    public function acceptChallenge($sender_id,$status)
    {
        if($status == 1){
            return $this->getModel()->where('sender_id',$sender_id)
                ->where('receiver_id',Auth::user()->id)
                ->where('complete_status',0)
                ->update(['complete_status' => 1]);
        }else{
            return $this->getModel()->where('sender_id',$sender_id)
                ->where('receiver_id',Auth::user()->id)
                ->where('complete_status',0)
                ->delete();
        }
    }
    public function deleteFbFriends($id)
    {
        $this->getModel()->where(['user_id' => $id])->delete();
    }
    public function getFriendsBets($filter_param,$number)
    {
        $fb_friends_table = $this->getModel()->getTable();
        $users_table = (new UsersRepository(new User()))->getModel()->getTable();
        $user_bets_Table = (new UsersBetsRepository(new UserBET()))->getModel()->getTable();

        $query = $this->getModel()->select(
            $user_bets_Table.'.sender_bet',$user_bets_Table.'.receiver_bet',$user_bets_Table.'.privacy_type',$user_bets_Table.'.end_date',
            'senders.first_name as sender_first_name','senders.last_name as sender_last_name','senders.picture as sender_picture',
            'receivers.first_name as receiver_first_name','receivers.last_name as receiver_last_name','receivers.picture as receiver_picture')
            ->where($fb_friends_table.'.user_id' , Auth::user()->id)
            ->where($user_bets_Table.'.id', '<>' ,null)
            ->where($user_bets_Table.'.privacy_type' , '<>' , 'Invite only')
            ->leftJoin($users_table." as senders",'senders.fb_id','=',$fb_friends_table.'.fb_id')
            ->leftJoin($user_bets_Table,'senders.id','=',$user_bets_Table.'.sender_id')
            ->leftJoin($users_table." as receivers",'receivers.id','=',$user_bets_Table.'.receiver_id')
            ->offset($number*25-25)
            ->limit(25);
        if($filter_param == 'all')
        {
            $query = $query->orderBy($user_bets_Table.'.created_at','desc');
            return $query->get();
        }
        if($filter_param == 'pending')
        {
//            $query = $query->where($user_bets_Table.'.complete_status' ,0)
//                ->orWhere($user_bets_Table.'.complete_status' ,1);


            $query = $query->where(function($query){
                $query->where('user_bets.complete_status' ,0);
                $query->orWhere('user_bets.complete_status',1);
            });

            return $query->get();
        }
        if($filter_param == 'completed')
        {
            $query = $query->where($user_bets_Table.'.complete_status' ,1)
                ->where($user_bets_Table.'.end_date','<=', date('Y-m-d'));
            return $query->get();
        }
        return  $query->orderBy($user_bets_Table.'.created_at','desc')->get();


    }


}





