<?php
/**
 * Created by PhpStorm.
 * user: nomantufail
 * Date: 10/10/2016
 * Time: 10:13 AM
 */

namespace App\Repositories;

use App\Events\UserRegistered;
use App\Libs\Auth\Auth;
use App\Models\BlockedUser;
use App\Models\InviteUser;
use App\Models\LikedUser;
use App\Models\Notification;
use App\Models\UserInterests;
use App\User;
use App\UserBET;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class UsersBetsRepository extends Repository
{
    public function __construct()
    {
        $this->setModel(new UserBET());
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function store($user)
    {
        return $this->getModel()->create($user);
    }




//    public function updateWhere($where, $data)
//    {
//        return $this->getModel()->where($where)->update($data);
//    }
    public function updateWhere($where, $attrs)
    {
        $query = $this->getModel();
        foreach ($where as $key => $value) {
            $query = $query->where($key, '=', $value);
        }
        return $query->update($attrs);
    }


    public function getByIds($ids = [])
    {
        $lat1 = Auth::user()->latitude;
        $lon1 = Auth::user()->longitude;
        $usersTable = $this->getModel()->getTable();
        return $this->getModel()->select("*", DB::raw("(((acos(sin((" . $lat1 . "*pi()/180)) *
            sin(($usersTable.latitude*pi()/180))+cos((" . $lat1 . "*pi()/180)) *
            cos(($usersTable.latitude*pi()/180)) * cos(((" . $lon1 . "- $usersTable.longitude)*
            pi()/180))))*180/pi())*60*1.1515) as distance"))
            ->whereIn('id', $ids)
            ->get();
    }

    public function findByEmail($email)
    {
        $usersTable = $this->getModel()->getTable();
        return $this->getModel()->where('email', $email)
            ->first();
    }

    public function findByUserName($user_name)
    {
        return $this->getModel()->where('user_name', $user_name)
            ->first();
    }


    public function findByFbId($fbid)
    {
        return $this->getModel()->where('fb_id', $fbid)->first();
    }

    public function findByToken($token)
    {
        $usersTable = $this->getModel()->getTable();
        return $this->getModel()->where('access_token', $token)
            ->first();
    }

    private function getUserTableFields()
    {
        $userFields = [];
        collect($this->getModel()->fields())->each(function ($value, $key) use (&$userFields) {
            $userFields[] = "users." . $value;
        });
        return $userFields;
    }
    public function checkOldPassword($old_password)
    {
        return $this->getModel()->select('password')->where('password',bcrypt($old_password))->first();
    }
    public function acceptChallenge($bet_id,$status)
    {
        if($status == 1){
            return $this->getModel()->where('id',$bet_id)
                ->where('receiver_id',Auth::user()->id)
                ->where('complete_status',0)
                ->update(['complete_status' => 1]);
        }else{
            return $this->getModel()->where('id',$bet_id)
                ->where('receiver_id',Auth::user()->id)
                ->where('complete_status',0)
                ->delete();
        }
    }

    public function getMyBets($filter_param,$number)
    {
        $usersTable = (new UsersRepository(new User()))->getModel()->getTable();
        $user_bets_Table = $this->getModel()->getTable();
        $query = $this->getModel()->select($user_bets_Table.'.sender_bet',$user_bets_Table.'.receiver_bet',$user_bets_Table.'.privacy_type',$user_bets_Table.'.end_date',
            'senders.first_name as sender_first_name','senders.last_name as sender_last_name','senders.picture as sender_picture',
            'receivers.first_name as receiver_first_name','receivers.last_name as receiver_last_name','receivers.picture as receiver_picture')
            ->where($user_bets_Table.'.sender_id', Auth::user()->id)
            ->leftJoin($usersTable.' as senders','senders.id','=',$user_bets_Table.'.sender_id')
            ->leftJoin($usersTable.' as receivers','receivers.id','=',$user_bets_Table.'.receiver_id')
            ->offset($number*25-25)
            ->limit(25);

        if($filter_param == 'all')
        {
            $query = $query->orderBy($user_bets_Table.'.created_at','desc');
            return $query->get();
        }
        if($filter_param == 'pending')
        {
            $query = $query->where(function($query){
                $query->where('user_bets.complete_status' ,0);
                $query->orWhere('user_bets.complete_status',1);
            });
            return $query->get();
        }
        if($filter_param == 'completed')
        {
            $query = $query->where(function($query){
                $query->where('user_bets.complete_status' ,1);
                $query->where('user_bets.end_date','<=', date('Y-m-d'));
            });
            return $query->get();
        }
        return  $query->orderBy($user_bets_Table.'.created_at','desc')->get();

    }







}





