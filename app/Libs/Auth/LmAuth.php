<?php
/**
 * Created by PhpStorm.
 * User: waqas
 * Date: 3/17/2016
 * Time: 12:08 PM
 */

namespace App\Libs\Auth;

use App\Repositories\UsersRepository;
use App\User;
use Illuminate\Support\Facades\Hash;
class LmAuth extends Auth
{

    /**
     * @param array $credentials
     * @return bool
     */
    public static function attempt(array $credentials)
    {
        $check = false;
        try{
            $user = (new UsersRepository())->findByEmail($credentials['email']);
            if($user == null){
                $check = false;
            }
            else{
                if(!Hash::check($credentials['password'], $user->password)){
                    $check = false;
                }
                else{
                    $check = true;
                }
            }
        }
        catch (\Exception $e){
            return false;
        }
        return $check;
    }

}