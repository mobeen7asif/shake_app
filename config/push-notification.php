<?php

return array(

    'ios' => array(
        'environment' =>'development',
        'certificate' =>config_path('pem/shake.pem'),
        'passPhrase'  =>'1234',
        'service'     =>'apns'
    ),
    'android' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyDpafHPJACrw1xTw9ZxUFUwXQlvqRJSISg',
        'service'     =>'gcm'
    )

);