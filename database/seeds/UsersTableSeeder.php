<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            ['id' => '1','first_name'=>'John','last_name'=>'itachi', 'email' => 'noman@gmail.com','picture'=> 'https://cdn.bandmix.co.uk/bandmix_uk/media/324/324371/340187-p.jpg?v=1489498074','fb_id'=>0, 'access_token' => '123','device_token' =>'123','device_type'=> 'android'],
            ['id' => '2','first_name'=>'Furgson','last_name'=>'itachi', 'email' => 'mobin@gmail.com','picture'=> 'https://cdn.bandmix.co.uk/bandmix_uk/media/324/324371/340187-p.jpg?v=1489498074','fb_id'=>0, 'access_token' => '12asd3','device_token' =>'123','device_type'=> 'android'],
            ['id' => '3','first_name'=>'Henry','last_name'=>'itachi', 'email' => 'ali@gmail.com','picture'=> 'https://cdn.bandmix.co.uk/bandmix_uk/media/324/324371/340187-p.jpg?v=1489498074','fb_id'=>0, 'access_token' => '1sd23','device_token' =>'123','device_type'=> 'android'],
            ['id' => '4','first_name'=>'Rooney','last_name'=>'itachi', 'email' => 'tobi@gmail.com','picture'=> 'https://cdn.bandmix.co.uk/bandmix_uk/media/324/324371/340187-p.jpg?v=1489498074','fb_id'=>0, 'access_token' => '12323','device_token' =>'123','device_type'=> 'android'],
        ]);}
}
